# Ejemplo inicial para Oled SDD1306 128x64 i2c#

### What is this repository for? ###

* Un ejemplo para empezar a usar la pantalla usando un cliente i2c
* Se inicializa la pantalla y se le manda una imagen

* Esta forma de trabajar no es la correcta, pero para hacer una prueba vale.
* Se debería registrar un driver cliente i2c que se active cuando esté presente en el sistema el dispositivo.

### How do I get set up? ###

* Makefile para compilar el módulo

### Who do I talk to? ###

* andres@uma.es